#include <Adafruit_Sensor.h>


#define BLYNK_TEMPLATE_ID "TMPLQ7uASIDD"
#define BLYNK_DEVICE_NAME "Data Monitoring with Automation"
#define BLYNK_AUTH_TOKEN "g71ukYqGH3yd1SEx0Mnw9J7b02xVCfst"
#define TCP_MSS whatever
#define LWIP_IPV6 whatever
#define LWIP_FEATURES whatever
#define LWIP_OPEN_SRC whatever
#define BLYNK_PRINT Serial
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp8266.h>

#include "DHT.h"

char auth[] = BLYNK_AUTH_TOKEN;

char ssid[] = "INPT_TEST";
char pass[] = "iinnpptt";

//BlynkTimer timer;

#define DHTPIN D1
#define DHTTYPE DHT11

DHT dht(DHTPIN,DHTTYPE);
int Temperature;
int Humidity;
BLYNK_WRITE(V2){
  int pinvalue= param.asInt();
  digitalWrite(D2,pinvalue);
  }
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  //dht.setup(D1);
 
  pinMode(D1,OUTPUT);
  pinMode(D2,OUTPUT);
  Blynk.begin(auth, ssid, pass);
  //dht.begin();
  //timer.setInterval(100L,sendSensor);
}

void loop() {
  digitalWrite(D2,HIGH);
  //delay(1000);
  // put your main code here, to run repeatedly:
  Blynk.run ();
  //timer.run ();
  Temperature=dht.readTemperature();
  Humidity=dht.readHumidity();
  //
   Serial.print("Temperature : ");
    Serial.print(Temperature);
    Serial.print("Humidity : ");
    Serial.print(Humidity);
     Blynk.virtualWrite(V0,Temperature);
    Blynk.virtualWrite(V1,Humidity);
    //delay(1000);
    digitalWrite(D2,LOW);
    delay(1000);

      
}
